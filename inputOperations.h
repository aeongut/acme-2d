#ifndef INPUTOPERATIONS_H_INCLUDED
#define INPUTOPERATIONS_H_INCLUDED

#include "grid.h"

class inputOperations
{
    private:
        ///     Input Variables         ///All read from input file
        char    geometryFile[16];       ///Used to store the name of the geometry file
        int     uniformDivision;        ///Number of divisions to generate a uniform mesh
        float   rootDimension;          ///Dimension of the root cell
        float   geometryOffset[2];      ///Offset to place geometry in the domain (x and y)
        float   geometryAngle;          ///Angle to rotate geometry (angle of attack)
        float   geometryScale;          ///Scaling factor to resize geometry

        ///     geometry variables      ///
        int     numberOfPolygons;       ///# of polygons in geometry(read from geometry file)
        int     *numberOfGeometryPoints;///# of points in geometry for each polygon(read from geometry file)
        coord3D **geometryPoints;       ///Array for the data points for each polygon of geometry
        coord2D geometryMax;            ///Maximum x and y dimensions of geometry
        coord2D geometryMin;            ///Minimum x and y dimensions of geometry

        ///     Input Functions         ///
        void    readInput();            ///Reads input file "input.txt"
        void    readGeometry();         ///Reads the geometry file specified in input file
        void    placeGeometry();        ///Places geometry in domain, also scales and rotates

    public:
        //ACCESS FUNCTION
        void    GetInputAndGeometry();
        //GETTERS
        int     Get_uniformDivision() {return uniformDivision;}                 ///Needed for mesh generation
        int     Get_rootDimension() {return rootDimension;}                     ///Needed for mesh generation
        int     Get_numberOfPolygons() {return numberOfPolygons;}               ///Needed for output
        int     Get_numberOfGeometryPoints(int i) {return numberOfGeometryPoints[i];}  ///Needed for output
        coord3D Get_geometryPoints(int i, int j) {return geometryPoints[i][j];}                 ///Needed for output
        coord2D Get_geometryMax() {return geometryMax;}                         ///not sure but probably for inout test
        coord2D Get_geometryMin() {return geometryMin;}                         ///not sure but probably for inout test
        //SETTERS : These are all determined privately, there is no need to reach them in public
        //void    Set_uniformDivision(int n_uniformDivision) {uniformDivision = n_uniformDivision;}
        //void    Set_rootDimension(int n_rootDimension) {rootDimension = n_rootDimension;}
        //void    Set_numberOfPolygons(int n_numberOfPolygons) {numberOfPolygons = n_numberOfPolygons;}
        //void    Set_numberOfGeometryPoints(int* n_numberOfGeometryPoints) {numberOfGeometryPoints = n_numberOfGeometryPoints;}
        //void    Set_geometryPoints(float*** n_geometryPoints) {geometryPoints = n_geometryPoints;}
        //void    Set_geometryMax(coord n_geometryMax) {geometryMax = n_geometryMax;}
        //void    Set_geometryMin(coord n_geometryMin) {geometryMin = n_geometryMin;}

};

#endif // INPUTOPERATIONS_H_INCLUDED
