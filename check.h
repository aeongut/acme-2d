#ifndef CHECK_H_INCLUDED
#define CHECK_H_INCLUDED

#include "cutgrid.h"
#include "inputOperations.h"

class check
{
    private:
        int     sublevel;
        void    numberCells(grid*);
        void    listLeafCells(grid*);
        void    listCorners(grid*);
        void    listCellType(grid*);
        void    listCutCells(grid*);
    public:
        void    checkControl(grid*,inputOperations*);
};



#endif // CHECK_H_INCLUDED
