#include "inputOperations.h"

enum { OFF, ON }; ///OFF=0, ON=1

void printComment(ifstream &file, int echo)
{
    string line;                            /// used to store the comment string
    file.ignore(256,'#');                   /// everything is ignored until specified character
    getline(file,line);                     /// then a line is stored in line string
    if(echo==ON) cout << setw(20) << line;  /// if echo is 1, comment is printed
}

void inputOperations::GetInputAndGeometry()
{
    readInput();
    readGeometry();
    placeGeometry();
    cout<< "**********  End of input operations.  **********"<< endl;
}

void inputOperations::readInput()
{
/// ---------------------------------------------------------------------------
/// preProcessor::readInput()
/// This function is used to read input values from an input file. In the
/// input file user can include comments until some specified character (for
/// example '#') and the print out of these are controlled by printComment
/// function.
/// ---------------------------------------------------------------------------
    ifstream inputFile;
    inputFile.open("input.txt",ios::in);
    int ECHO=OFF;
    cout.precision(7);
    cout << scientific;

    printComment(inputFile, ECHO);
    inputFile >> ECHO;                              /// if ECHO is ON(=1), comments are displayed

    printComment(inputFile, ECHO);
    inputFile >> geometryFile;
    if(ECHO==ON) cout << ' ' << geometryFile << endl;

    printComment(inputFile, ECHO);
    inputFile >> rootDimension;
    if(ECHO==ON) cout << ' ' << rootDimension << endl;

    printComment(inputFile, ECHO);
    inputFile >> uniformDivision;
    if(ECHO==ON) cout << ' ' << uniformDivision << endl;

    printComment(inputFile, ECHO);
    inputFile >> geometryOffset[0];
    if(ECHO==ON) cout << ' ' << geometryOffset[0] << endl;

    printComment(inputFile, ECHO);
    inputFile >> geometryOffset[1];
    if(ECHO==ON) cout << ' ' << geometryOffset[1] << endl;

    printComment(inputFile, ECHO);
    inputFile >> geometryScale;
    if(ECHO==ON) cout << ' ' << geometryScale << endl;

    printComment(inputFile, ECHO);
    inputFile >> geometryAngle;
    if(ECHO==ON) cout << ' ' << geometryAngle<< endl;
}

void inputOperations::readGeometry()
{
/// ---------------------------------------------------------------------------
/// preProcessor::readGeometry()
/// Here a geometry file in simple VTK format is opened and data points are
/// extracted from that file. Polygon type data points are assumed for the
/// supplied vtk format geometry file.
/// ---------------------------------------------------------------------------
    int         i,j,k;                /// are used in loops
    int         tempNumberOfPoints;     /// temporarily stores number of points
    coord3D     *tempPoints;            /// temporarily stores points before moving to global var
    float       dummy;
    char        c_line[256];            /// dummy
    ifstream    inputGeometry;          /// file stream var

    inputGeometry.open(geometryFile,ios::in);

    /// find the number of points and read
    for(i=0; i<4; i++)
        inputGeometry.getline(c_line,256,'\n');
    inputGeometry.ignore(20,' ');
    inputGeometry >> tempNumberOfPoints;
    /// allocate temporary space for x,y,z coordinates of each point
    tempPoints = new coord3D [tempNumberOfPoints];

    /// get to the next line  and read coordinates of each point
    inputGeometry.getline(c_line,256,'\n');
    for(i=0; i<tempNumberOfPoints; i++)
    {
        inputGeometry >> dummy;
            tempPoints[i].Set_x(dummy);
        inputGeometry >> dummy;
            tempPoints[i].Set_y(dummy);
        inputGeometry >> dummy;
            tempPoints[i].Set_z(dummy);
    }

    /// find the number of polygons, read and allocate numberOfGeometryPoints
    inputGeometry.ignore(20,' ');
    inputGeometry >> numberOfPolygons;
    numberOfGeometryPoints = new int [numberOfPolygons];

    ///Find out how many points each polygon have
    inputGeometry.getline(c_line,256,'\n');
    for(i=0; i<numberOfPolygons; i++)
    {
        inputGeometry >> numberOfGeometryPoints[i];
        inputGeometry.getline(c_line,256,'\n');
    }

    ///allocate space for points : [numberOfPolygons] [numberOfGeometryPoints]
    geometryPoints = new coord3D* [numberOfPolygons];
    for(i=0; i<numberOfPolygons; i++)
        geometryPoints[i] = new coord3D [numberOfGeometryPoints[i]];

    ///Copy point data from tempPoints to geometryPoints for each polygon
    for(i=0,k=0; i<numberOfPolygons; i++)
        for(j=0; j<numberOfGeometryPoints[i]; j++,k++)
        {
            geometryPoints[i][j].Set_x(tempPoints[k].Get_x());
            geometryPoints[i][j].Set_y(tempPoints[k].Get_y());
            geometryPoints[i][j].Set_z(tempPoints[k].Get_z());
        }
    delete tempPoints;
}


void inputOperations::placeGeometry()
{
/// ---------------------------------------------------------------------------
/// preProcessor::placeGeometry()
/// The geometry can be rotated scaled and moved in the domain using this
/// function. geometryAngle, geometryScale, geometryOffset[2] vars determines
/// corresponding geometric operations.
/// ---------------------------------------------------------------------------

    ///rotation
    float angle = PI*geometryAngle/180.0f;
    if(geometryAngle != 0.0)
        for(int i=0; i<numberOfPolygons; i++)
            for(int j=0; j<numberOfGeometryPoints[i]; j++)
            {
                float x = geometryPoints[i][j].Get_x();
                float y = geometryPoints[i][j].Get_y();
                geometryPoints[i][j].Set_x(x*cos(angle)-y*sin(angle));
                geometryPoints[i][j].Set_y(x*sin(angle)+y*cos(angle));
            }

    ///scaling
    if(geometryScale != 1.0)
        for(int i=0; i<numberOfPolygons; i++)
            for(int j=0; j<numberOfGeometryPoints[i]; j++)
            {
                geometryPoints[i][j].Set_x(geometryPoints[i][j].Get_x() * geometryScale);
                geometryPoints[i][j].Set_y(geometryPoints[i][j].Get_y() * geometryScale);
            }

    ///translation
    if(geometryOffset[0]!=0.0 || geometryOffset[1]!=0.0)
        for(int i=0; i<numberOfPolygons; i++)
            for(int j=0; j<numberOfGeometryPoints[i]; j++)
            {
                geometryPoints[i][j].Set_x(geometryPoints[i][j].Get_x() + geometryOffset[0]);
                geometryPoints[i][j].Set_y(geometryPoints[i][j].Get_y() + geometryOffset[1]);
            }

    ///after all operations max and min coordinates of geometry can be found
    geometryMax.Set_x(0.0f);
    geometryMax.Set_y(0.0f);
    geometryMin.Set_x(rootDimension);
    geometryMin.Set_y(rootDimension);
    for(int i=0; i<numberOfPolygons; i++)
        for(int j=0; j<numberOfGeometryPoints[i]; j++)
            {
                if(geometryPoints[i][j].Get_x() > geometryMax.Get_x())
                    geometryMax.Set_x(geometryPoints[i][j].Get_x());
                if(geometryPoints[i][j].Get_y() > geometryMax.Get_y())
                    geometryMax.Set_y(geometryPoints[i][j].Get_y());

                if(geometryPoints[i][j].Get_x() < geometryMin.Get_x())
                    geometryMin.Set_x(geometryPoints[i][j].Get_x());
                if(geometryPoints[i][j].Get_y() < geometryMin.Get_y())
                    geometryMin.Set_y(geometryPoints[i][j].Get_y());
            }
}

