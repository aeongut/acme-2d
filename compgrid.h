#ifndef COMPGRID_H_INCLUDED
#define COMPGRID_H_INCLUDED

#include "grid.h"

class compgrid : public grid
{
    private:
        int temperature;  ///This can be any property actually

    public:
        compgrid ();             /// DEFAULT CONSTRUCTOR
        compgrid (grid* cell);   /// COMPGRID FROM CELL CONSTRUCTOR
        ~compgrid ();            /// DESTRUCTOR

    /// GETTERS ///
        int Get_temperature() {return temperature;}

    /// SETTERS ///
        void Set_temperature(int n_temperature) {temperature = n_temperature;}
};


#endif // COMPGRID_H_INCLUDED
