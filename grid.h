#ifndef GRID_H_INCLUDED
#define GRID_H_INCLUDED

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <cmath>

#include "coord2D.h"
#include "coord3D.h"
#include "constants.h"

using namespace std;

class grid
{
    protected:
    // VARIABLES
        int     cellID;
        int     cellType;
        int     level;
        float   length;
        float   property;
        coord2D centerCoord;
        coord2D cornerCoord[4];
        grid    *parent;
        grid    *child[4];
        grid    *ng[4];


    public:
        grid();             /// DEFAULT CONSTRUCTOR
        grid(grid* cell);   /// COPY CONSTRUCTOR
        ~grid();            /// DESTRUCTOR

    /// GETTERS ///
        int         Get_cellID      ()      {return cellID;}
        int         Get_cellType    ()      {return cellType;}
        int         Get_level       ()      {return level;}
        float       Get_length      ()      {return length;}
        float       Get_property    ()      {return property;}
        coord2D&    Get_centerCoord ()      {return centerCoord;}
        coord2D&    Get_cornerCoord (int i) {return cornerCoord[i];}
        grid*       Get_parent      ()      {return parent;}
        grid*       Get_child       (int i) {return child[i];}
        grid*       Get_ng          (int i) {return ng[i];}

    /// SETTERS ///
        void    Set_cellID      (int n_cellID)              {cellID = n_cellID;}
        void    Set_cellType    (int n_cellType)            {cellType = n_cellType;}
        void    Set_level       (int n_level)               {level = n_level;}
        void    Set_length      (float n_length)            {length = n_length;}
        void    Set_property    (float n_property)          {property = n_property;}
        void    Set_centerCoord (coord2D n_center)          {centerCoord = n_center;}
        void    Set_cornerCoord (int i, coord2D n_corner)   {cornerCoord[i] = n_corner;}
        void    Set_parent      (grid* n_parent)            {parent = n_parent;}
        void    Set_child       (int i, grid* n_child)      {child[i] = n_child;}
        void    Set_ng          (int i, grid* n_ng)         {ng[i] = n_ng;}



/*************************************************************************
*     _______________
*    |       |       |
*    |child1 |child0 |
*    |       |       |
*    |_______|_______|
*    |       |       |
*    |child2 |child3 |
*    |       |       |
*    |_______|_______|
*
*         __ng1__
*        |       |
*   ng2  |       | ng0
*        |       |
*        |_______|
*           ng3
*
*************************************************************************/
 };


#endif // GRID_H_INCLUDED
