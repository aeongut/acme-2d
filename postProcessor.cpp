#include "postProcessor.h"

void postProcessor::outputToVTK(grid *cell, inputOperations *IO)
{
/// ---------------------------------------------------------------------------
/// postProcessor::outputToVTK
///
/// ---------------------------------------------------------------------------

    ///Necessary data is collected here.
    int noPoints=0;     ///Total number of all the geometry points in all polygons
    int totalPoints=0;  ///noPoints + number of all the corners of all the mesh elements
    int leafs = countLeafCells(cell);   ///Number of leaf cells are determined
    for(int i=0; i<IO->Get_numberOfPolygons();i++)  ///noPoints is determined
        noPoints = noPoints + IO->Get_numberOfGeometryPoints(i);
    totalPoints = 4*leafs+noPoints;

    vtkFloatArray* pcoords = vtkFloatArray::New();
    pcoords->SetNumberOfComponents(3);
    pcoords->SetNumberOfTuples(totalPoints);

    ///MeshPoints is a 2D array, it is going to be filled with mesh points and geometry points sequentially.
    float** meshPoints = new float* [totalPoints];
    for(int i=0; i<totalPoints; i++)
        meshPoints[i] = new float [3];

    ///This function fills mesh coordinates to meshPoints 2d array.
    outputToVTKWriteMeshCoords(cell, meshPoints);

    ///This function fills geometry coordiantes to meshPoints 2d array.
    ///It continues writing from the place left by the outputToVTKWriteMeshCoords fuction above.
    outputToVTKWriteGeomCoords(IO, meshPoints, leafs);

    ///vtkFloatArray type pcoords is filled with the data in meshPoints.
    for (int i=0; i<totalPoints; i++)
        pcoords->SetTuple(i, meshPoints[i]);

    ///vtkPoints type outputPoints is filled with the data in pcoords.
    vtkPoints* outputPoints = vtkPoints::New();
    outputPoints->SetData(pcoords);

    ///Connectivity of mesh cells and geometry is written to vtkCellArray type outputCells var by below function.
    vtkCellArray* outputCells = vtkCellArray::New();
    outputToVTKWriteConnectivity(IO, outputCells, leafs);

    ///cellType sclar property is written to vtkFloatArray type scalarProperty var by below function.
    ///IMPORTANT: If another scalar is desired to be added, this part and the corresponding function should be
    ///added as well. Maybe a shorter way should be considered.
    vtkFloatArray* scalarProperty = vtkFloatArray::New();
    scalarProperty->SetName("CellType");
    outputToVTKWriteProperty(cell, scalarProperty);

    ///Previously collected data which are outputPoints, outputCells, scalarProperty, are written to vtkPolyData type
    ///polydata var.
    vtkPolyData* polydata = vtkPolyData::New();
    polydata->SetPoints(outputPoints);
    polydata->SetPolys(outputCells);
    polydata->GetCellData()->SetScalars(scalarProperty);

    ///Whatever collected in polydata above is written to the vtk file below.
    vtkDataSetWriter *writer = vtkDataSetWriter::New();
    writer->SetFileName("Mesh_and_Geometry.vtk");
    writer->SetInput(polydata);
    writer->Write();

    ///In the below section, data collected in polydata var is rendered.
    // Create the mapper and set the appropriate scalar range
    // (default is (0,1)
    vtkPolyDataMapper* mapper = vtkPolyDataMapper::New();
    mapper->SetInput(polydata);
    mapper->SetScalarRange(0, 2);

    // Create an actor.
    vtkActor* actor = vtkActor::New();
    actor->SetMapper(mapper);

    // Create the rendering objects.
    vtkRenderer* ren = vtkRenderer::New();
    ren->AddActor(actor);

    vtkRenderWindow* renWin = vtkRenderWindow::New();
    renWin->AddRenderer(ren);

    vtkRenderWindowInteractor* iren = vtkRenderWindowInteractor::New();
    iren->SetRenderWindow(renWin);
    iren->Initialize();
    iren->Start();

    ///Unnecesary data is destroyed.
    delete [] meshPoints;
    pcoords->Delete();
    outputPoints->Delete();
    outputCells->Delete();
    scalarProperty->Delete();
    polydata->Delete();
    mapper->Delete();
    actor->Delete();
    ren->Delete();
    renWin->Delete();
    iren->Delete();

    cout<< "**********  End of post-process operations.  **********"<< endl;
}


void postProcessor::outputToVTKWriteMeshCoords(grid* cell, float** meshPts)
{
/// ---------------------------------------------------------------------------
/// postProcessor::outputToVTKWriteMeshCoords
///
/// ---------------------------------------------------------------------------

    static int meshIndex=0;

    if(cell->Get_child(0) != NULL)
        for(int i=0; i<4; i++)
            outputToVTKWriteMeshCoords(cell->Get_child(i), meshPts);
    else
    {
        for(int j=0; j<4; j++)
        {
            meshPts[meshIndex][0] = cell->Get_cornerCoord(j).Get_x();
            meshPts[meshIndex][1] = cell->Get_cornerCoord(j).Get_y();
            meshPts[meshIndex][2] = 0.0f;
            meshIndex++;
        }
    }
}

void postProcessor::outputToVTKWriteGeomCoords(inputOperations *IO, float** meshPts, int leafs)
{
/// ---------------------------------------------------------------------------
/// postProcessor::outputToVTKWriteGeomCoords
///
/// ---------------------------------------------------------------------------

    int geomIndex=4*leafs;
    for(int i=0; i<IO->Get_numberOfPolygons(); i++)
    {
        for(int j=0; j<IO->Get_numberOfGeometryPoints(i); j++)
        {
            {
                meshPts[geomIndex][0] = IO->Get_geometryPoints(i,j).Get_x();
                meshPts[geomIndex][1] = IO->Get_geometryPoints(i,j).Get_y();
                meshPts[geomIndex][2] = IO->Get_geometryPoints(i,j).Get_z();
                geomIndex++;
            }
        }
    }
}

void postProcessor::outputToVTKWriteConnectivity(inputOperations *IO, vtkCellArray* cellArray, int leafs)
{
/// ---------------------------------------------------------------------------
/// postProcessor::outputToVTKWriteConnectivity
///
/// ---------------------------------------------------------------------------

    for(int i=0; i<leafs; i++)
    {
        cellArray->InsertNextCell(4);
        cellArray->InsertCellPoint(4*i);
        cellArray->InsertCellPoint(4*i+1);
        cellArray->InsertCellPoint(4*i+2);
        cellArray->InsertCellPoint(4*i+3);
    }

    int start=4*leafs;
    for(int i=0; i<IO->Get_numberOfPolygons(); i++)
    {
        cellArray->InsertNextCell(IO->Get_numberOfGeometryPoints(i));
        for(int j=0; j<IO->Get_numberOfGeometryPoints(i); j++)
            cellArray->InsertCellPoint(start+j);
        start = start + IO->Get_numberOfGeometryPoints(i);
    }
}

void postProcessor::outputToVTKWriteProperty(grid *cell, vtkFloatArray* property)
{
/// ---------------------------------------------------------------------------
/// postProcessor::outputToVTKWriteProperty
///
/// ---------------------------------------------------------------------------

    if(cell->Get_child(0) != NULL)
        for(int i=0; i<4; i++)
            outputToVTKWriteProperty(cell->Get_child(i), property);
    else
        property->InsertNextValue(cell->Get_cellType());
}

int postProcessor::countLeafCells(grid *cell)
{
/// ---------------------------------------------------------------------------
/// postProcessor::countLeafCells
///
/// ---------------------------------------------------------------------------

    static int counter = 0;

    if(cell->Get_child(0) != NULL)
        for(int i=0; i<4; i++)
            this->countLeafCells(cell->Get_child(i));
    else
        counter++;

    return counter;
}
