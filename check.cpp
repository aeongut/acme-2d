#include "check.h"

void check::checkControl(grid *cell, inputOperations *IO)

{
    sublevel = IO->Get_uniformDivision();
    numberCells(cell);
//    listLeafCells(cell);
//    listCorners(cell);
//    listCellType(cell);
//    listCutCells(cell);
}

/// ---------------------------------------------------------------------------
/// check::numberLeafCells
/// This function numbers cells at "sublevel" depth recursively.
/// ---------------------------------------------------------------------------
void check::numberCells(grid *cell)

{
    static int ID = 0;

    if (cell->Get_level() == sublevel)
    {
        cell->Set_cellID(ID);
        ID++;
    }
    else
        for(int i=0; i<4; i++)
            numberCells(cell->Get_child(i));
}

/// ----------------------------------------------------------------------------
/// check::listLeafCells
/// This function prints cellIDs of a cell and its neighbours to help user to
/// check the creation of negihbours correctly.
///  ---------------------------------------------------------------------------
void check::listLeafCells(grid *cell)
{
    if (cell->Get_child(0) == NULL)
    {
        cout << cell->Get_cellID() << '\t';
        for(int a=0; a<4; a++)
            if(cell->Get_ng(a) == NULL)
                cout << "-1" << '\t';
            else
                cout << cell->Get_ng(a)->Get_cellID() << '\t';
        cout << endl;
    }
    else
        for(int i=0; i<4; i++)
            listLeafCells(cell->Get_child(i));
}

void check::listCorners(grid *cell)

{
    if (cell->Get_child(0) == NULL)
    {
        cout << cell->Get_cellID() << '\t';
        for(int a=0; a<4; a++)
            cout << cell->Get_cornerCoord(a).Get_x() << '\t' << cell->Get_cornerCoord(a).Get_y() << '\t';
        cout << endl;
    }
    else
        for(int i=0; i<4; i++)
            listCorners(cell->Get_child(i));
}

void check::listCellType(grid *cell)
{
    if (cell->Get_child(0) == NULL)
    {
        cout << cell->Get_cellID() << '\t';
        cout << cell->Get_cellType() << '\t' << endl;
    }
    else
        for(int i=0; i<4; i++)
            listCellType(cell->Get_child(i));
}

void check::listCutCells(grid *cell)
{
    if (cell->Get_child(0) == NULL)
    {
        if (cell->Get_cellID() == 9)
        {
            cout << "cellID : " << cell->Get_parent()->Get_child(2)->Get_cellID() << endl;
            cout << "cellType : " << cell->Get_parent()->Get_child(2)->Get_cellType() << endl;
            cout << "level : " << cell->Get_parent()->Get_child(2)->Get_level() << endl;
            cout << "property : " << cell->Get_parent()->Get_child(2)->Get_property() << endl;
            cout << "center x : " << cell->Get_parent()->Get_child(2)->Get_centerCoord().Get_x() << endl;
            cout << "center y : " << cell->Get_parent()->Get_child(2)->Get_centerCoord().Get_y() << endl;
            cout << endl;
        }
    }
    else
        for(int i=0; i<4; i++)
            listCutCells(cell->Get_child(i));

    /*
    if (cell->Get_child(0) == NULL)
    {
        if (cell->Get_cellType() == typeCut)
        {
            cout << "cellID : " << cell->Get_cellID() << endl;
            cout << "cellType : " << cell->Get_cellType() << endl;
            cout << "level : " << cell->Get_level() << endl;
            cout << "property : " << cell->Get_property() << endl;
            cout << "center x : " << cell->Get_centerCoord().Get_x() << endl;
            cout << "center y : " << cell->Get_centerCoord().Get_y() << endl;
            cutgrid *cutcell = static_cast<cutgrid*>(cell);
            cout << "Corner 0 type : " << cutcell->Get_cornerType(0) << endl;
            cout << endl;
        }
    }
    else
        for(int i=0; i<4; i++)
            listCutCells(cell->Get_child(i));
    */
}
