#ifndef CUTGRID_H_INCLUDED
#define CUTGRID_H_INCLUDED

#include "grid.h"

class cutgrid : public grid
{
    private:
        int cornerType[4];  ///As defined in constants.h file Out = 0, In = 1, Cut = 2.

    public:
        cutgrid ();             /// DEFAULT CONSTRUCTOR
        cutgrid (grid* cell);   /// CUTGRID FROM CELL CONSTRUCTOR
        ~cutgrid ();            /// DESTRUCTOR

    /// GETTERS ///
        int Get_cornerType(int index) {return cornerType[index];}

    /// SETTERS ///
        void Set_cornerType(int index, int n_cornerType) {cornerType[index] = n_cornerType;}
};

#endif // CUTGRID_H_INCLUDED
