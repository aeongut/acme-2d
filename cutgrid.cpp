#include "cutgrid.h"

/// DEFAULT CONSTRUCTOR
/// : grid() is used to call the default constructor of the grid class.
/// Members coming from the base class is constructed with the default constructor of base class.
cutgrid::cutgrid () : grid()
{
    for(int i=0;i<4;i++)
        cornerType[i] = -2;
}

/// CUTGRID FROM CELL CONSTRUCTOR
/// Difference from the default constructor is that it first calls the grid(grid*) constructor
/// of the base class. This base class constructor copies the common members of the grid object
/// to cutgrid object. It also arranges all the parent, child and neigbbouring relations according
/// to the new cutgrid object.
/// : grid(grid*) is used to call the copy constructor of the grid class
cutgrid::cutgrid (grid* cell) : grid(cell)
{
    ///Construct the variables
    for(int i=0;i<4;i++)
        cornerType[i] = -2;
}

/// DESTRUCTOR
cutgrid::~cutgrid ()
{
    delete [] cornerType;
}
