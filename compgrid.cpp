#include "compgrid.h"

/// DEFAULT CONSTRUCTOR
/// : grid() is used to call the default constructor of the grid class
/// Members coming from the base class is constructed with the default constructor of base class.
compgrid::compgrid () : grid()
{
    temperature = -2.0;
}

/// COMPGRID FROM CELL CONSTRUCTOR
/// Difference from the default constructor is that it first calls the grid(grid*) constructor
/// of the base class. This base class constructor copies the common members of the grid object
/// to cutgrid object. It also arranges all the parent, child and neigbbouring relations according
/// to the new cutgrid object.
/// : grid(grid*) is used to call the copy constructor of the grid class
compgrid::compgrid (grid* cell) : grid(cell)
{
    ///Construct the variables
    temperature = -2.0;
}

/// DESTRUCTOR
compgrid::~compgrid ()
{

}
