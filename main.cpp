/**
2D Adaptive Cartesian Grid Generator
Alp Emre Öngüt
latest update: 30 October 2010
**/

#include "grid.h"
#include "inputOperations.h"
#include "preProcessor.h"
#include "solver.h"
#include "postProcessor.h"
#include "check.h"

int main()
{

    grid            *root               = new grid;
    inputOperations *inputOperationsObj = new inputOperations;
    preProcessor    *preProcessorObj    = new preProcessor;
    solver          *solverObj          = new solver;
    postProcessor   *postProcessorObj   = new postProcessor;
    check           *checkObj           = new check;

    inputOperationsObj->GetInputAndGeometry();
    preProcessorObj->initializeControl(root, inputOperationsObj);
    postProcessorObj->outputToVTK(root, inputOperationsObj);

//    checkObj->checkControl(root, inputOperationsObj);


        return 0;
}
