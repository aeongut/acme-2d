#include "preProcessor.h"

void preProcessor::initializeControl (grid* root, inputOperations* IO)
{
/// ---------------------------------------------------------------------------
/// initializeClass::initializeControl
///    This function is used for the control the calls to functions in the
///    initializeClass.
/// ---------------------------------------------------------------------------

    /// Root Dimension and Number of Unifor Divisions are copied to local vars for simplicity
    float rootDimension = IO->Get_rootDimension();
    float uniformDivision = IO->Get_uniformDivision();

    /// Root cell is initialized
    initializeRoot(root, rootDimension);

    /// Uniform grid is generated to uniformDivision depth
    uniformGrid(root, uniformDivision);

    /// Neighbouring relations between all cells are determined
    findNeighbours(root);

    /// Corner coordinates of all cells are determined
    findCorners(root);

    /// Determines which cells are inside of, otside of or cut by geometry
	inoutTest(root, IO);

    /// Information line to stdout
    cout << "**********  End of pre-process operations.  **********" << endl;
}

void preProcessor::initializeRoot (grid *cell, const float rootDimension)
{
/// ---------------------------------------------------------------------------
/// initializeClass::initializeRoot
/// This function is used for the initialization of root cell only.
/// It takes the pointer to root cell and the side length of root cell.
/// ---------------------------------------------------------------------------

    cell->Set_level(0);                             /// Root cell is level zero.
    cell->Set_length(rootDimension);
    cell->Get_centerCoord().Set_x(rootDimension/2.0f);      /// Left bottom corner is the origin.
    cell->Get_centerCoord().Set_y(rootDimension/2.0f);
    cell->Get_cornerCoord(NE).Set_x(rootDimension);         /// North East corner cooridantes
    cell->Get_cornerCoord(NE).Set_y(rootDimension);
    cell->Get_cornerCoord(NW).Set_x(0.0f);                  /// North West corner coordinates
    cell->Get_cornerCoord(NW).Set_y(rootDimension);
    cell->Get_cornerCoord(SW).Set_x(0.0f);                  /// South West corner coordinates
    cell->Get_cornerCoord(SW).Set_y(0.0f);
    cell->Get_cornerCoord(SE).Set_x(rootDimension);         /// South East corner coordinates
    cell->Get_cornerCoord(SE).Set_y(0.0f);
    cell->Set_property(0.0f);
    cell->Set_parent(NULL);                         /// Root cell does not have parent.
    for(int i=0; i<4; i++)
    {
        cell->Set_child(i, NULL);       /// Children are initialized as NULL pointers.
        cell->Set_ng(i, NULL);          /// Neighbours of root cell are NULL pointers.
    }

}

void preProcessor::uniformGrid (grid *cell, const float uniformDivision)
{
/// ---------------------------------------------------------------------------
/// initializeClass::uniformGrid
/// This function is used to generate uniform grid with elements having
/// maximum level of division.
/// ---------------------------------------------------------------------------
    if(cell->Get_level() < uniformDivision)
    {
        float newlength = cell->Get_length()/2.0f;                /// New children have half length of their parent.
        float delta = cell->Get_length()/4.0f;                    /// Used to determine the center of children.
        for(int i=0; i<4; i++)                                    /// For for children...
        {
            if(cell->Get_level() == uniformDivision-1)              /// If we have a computational cell
                cell->Set_child(i, new compgrid);                   /// Allocate space for compgrid type children
            else                                                    /// If we have an ordinary cell
                cell->Set_child(i, new grid);                       /// Allocate space for grid type children
            cell->Get_child(i)->Set_parent(cell);                   /// Sets parent
            cell->Get_child(i)->Set_level(cell->Get_level()+1);     /// Sets level to one more of parents' level
            cell->Get_child(i)->Set_length(newlength);              /// Sets the length of children to half of parents'
            cell->Get_child(i)->Set_property(1.0f);                 /// Sets some property

            /// Centers of new cells determined here. A seperate function for uniform grid center determination
            /// could also be written.
            if(i==0)
            {
                cell->Get_child(i)->Get_centerCoord().Set_x(cell->Get_centerCoord().Get_x()+delta);
                cell->Get_child(i)->Get_centerCoord().Set_y(cell->Get_centerCoord().Get_y()+delta);
            }
            if(i==1)
            {
                cell->Get_child(i)->Get_centerCoord().Set_x(cell->Get_centerCoord().Get_x()-delta);
                cell->Get_child(i)->Get_centerCoord().Set_y(cell->Get_centerCoord().Get_y()+delta);
            }
            if(i==2)
            {
                cell->Get_child(i)->Get_centerCoord().Set_x(cell->Get_centerCoord().Get_x()-delta);
                cell->Get_child(i)->Get_centerCoord().Set_y(cell->Get_centerCoord().Get_y()-delta);
            }
            if(i==3)
            {
                cell->Get_child(i)->Get_centerCoord().Set_x(cell->Get_centerCoord().Get_x()+delta);
                cell->Get_child(i)->Get_centerCoord().Set_y(cell->Get_centerCoord().Get_y()-delta);
            }
            uniformGrid(cell->Get_child(i), uniformDivision); /// Recursive: sends every child again to uniformGrid.
        }
    }
    else                                                    /// If maximum depth is reached children are set to NULL.
        for(int i=0; i<4; i++)
            cell->Set_child(i, NULL);
}


void preProcessor::findNeighbours(grid *cell)
{
/// ---------------------------------------------------------------------------
/// initializeClass::findNeighbours
/// The aim of this function is to find the neighbouring relations in UNIFORM
/// GRID. Function is called for each level of mesh starting from level
/// one (ie.sublevel=1) by incrementing level by 1 until uniformDivision is
/// reached. At each level, function visits the cells at that level recurively
/// determining the neighbours of each cell.
/// ---------------------------------------------------------------------------

    ///If cell is the root cell and has children, its 4 children are sent to findNeigbours(grid*)
    if(cell->Get_level() == 0 && cell->Get_child(0) != NULL)
        for(int i=0; i<4; i++)
            findNeighbours(cell->Get_child(i));

    ///If cell is not the root cell, neighbouring relations are determined
    else
    {
        /// if I am the child[0] of my parent
        if(cell == cell->Get_parent()->Get_child(NE))
        {
            /// East neighbour ///
            if (cell->Get_parent()->Get_ng(E) == NULL) /// if east side is empty
                cell->Set_ng(E, NULL);
            else /// else my parent's E neighbour's NW child is my E neighbour
                cell->Set_ng(E, cell->Get_parent()->Get_ng(E)->Get_child(NW));

            /// North neighbour ///
            if (cell->Get_parent()->Get_ng(N) == NULL) /// if north side is empty
                cell->Set_ng(N, NULL);
            else /// else my parent's N neighbour's SE child is my N neighbour
                cell->Set_ng(N, cell->Get_parent()->Get_ng(N)->Get_child(SE));

            /// West neighbour ///
            cell->Set_ng(W, cell->Get_parent()->Get_child(NW));

            /// South neighbour ///
            cell->Set_ng(S, cell->Get_parent()->Get_child(SE));
        }

        /// if I am the child[1] of my parent
        if(cell == cell->Get_parent()->Get_child(NW))
        {
            cell->Set_ng(E, cell->Get_parent()->Get_child(NE));                 /// East neighbour
            if(cell->Get_parent()->Get_ng(N) == NULL)                           /// North neighbour
                cell->Set_ng(N, NULL);
            else
                cell->Set_ng(N, cell->Get_parent()->Get_ng(N)->Get_child(SW));
            if (cell->Get_parent()->Get_ng(W) == NULL)                          /// West neighbour
                cell->Set_ng(W, NULL);
            else
                cell->Set_ng(W, cell->Get_parent()->Get_ng(W)->Get_child(NE));
            cell->Set_ng(S, cell->Get_parent()->Get_child(SW));                 /// South neighbour
        }

       /// if I am the child[2] of my parent
       if(cell == cell->Get_parent()->Get_child(SW))
        {
            cell->Set_ng(E, cell->Get_parent()->Get_child(SE));                  /// East neighbour
            cell->Set_ng(N, cell->Get_parent()->Get_child(NW));                  /// North neighbour
            if (cell->Get_parent()->Get_ng(W) == NULL)                          /// West neighbour
                cell->Set_ng(W, NULL);
            else
                cell->Set_ng(W, cell->Get_parent()->Get_ng(W)->Get_child(SE));
            if (cell->Get_parent()->Get_ng(S) == NULL)                          /// South neighbour
                cell->Set_ng(S, NULL);
            else
                cell->Set_ng(S, cell->Get_parent()->Get_ng(S)->Get_child(NW));
        }

        /// if I am the child[3] of my parent
        if(cell == cell->Get_parent()->Get_child(SE))
        {
            if(cell->Get_parent()->Get_ng(E) == NULL)                           /// East neighbour
                cell->Set_ng(E, NULL);
            else
                cell->Set_ng(E, cell->Get_parent()->Get_ng(E)->Get_child(SW));
            cell->Set_ng(N, cell->Get_parent()->Get_child(NE));                  /// North neighbour
            cell->Set_ng(W, cell->Get_parent()->Get_child(SW));                  /// West neighbour
            if(cell->Get_parent()->Get_ng(S) == NULL)                           /// South neighbour
                cell->Set_ng(S, NULL);
            else
                cell->Set_ng(S, cell->Get_parent()->Get_ng(S)->Get_child(NE));
        }

        ///If the cell has children, they are sent to findNeighbours(grid*)
        if (cell->Get_child(0) != NULL)
            for(int i=0; i<4; i++)
                findNeighbours(cell->Get_child(i));
    }
}


void preProcessor::findCorners(grid *cell)
{
/// ---------------------------------------------------------------------------
/// initializeClass::findCorners
/// The aim of this function is to find the corner coordinates of all the
/// ---------------------------------------------------------------------------

    ///If cell is the root cell and has children, its 4 children are sent to findCorners(grid*)
    if(cell->Get_level() == 0 && cell->Get_child(0) != NULL)
        for(int i=0; i<4; i++)
            findCorners(cell->Get_child(i));

    ///If cell is not the root cell, corner coordinates are determined
    else
    {
        /// if I am the child[0] of my parent
        if(cell == cell->Get_parent()->Get_child(NE))
        {
            cell->Set_cornerCoord(NE, cell->Get_parent()->Get_cornerCoord(NE));   /// NE corner is common with parent's NE corner
            cell->Set_cornerCoord(SW, cell->Get_parent()->Get_centerCoord());     /// SW corner is common with parent's center

            cell->Get_cornerCoord(NW).Set_x(cell->Get_cornerCoord(SW).Get_x());   /// NWx = SWx
            cell->Get_cornerCoord(NW).Set_y(cell->Get_cornerCoord(NE).Get_y());   /// NWy = NEy

            cell->Get_cornerCoord(SE).Set_x(cell->Get_cornerCoord(NE).Get_x());   /// SEx = NEx
            cell->Get_cornerCoord(SE).Set_y(cell->Get_cornerCoord(SW).Get_y());   /// SEy = SWy
        }

        /// if I am the child[1] of my parent
        if(cell == cell->Get_parent()->Get_child(NW))
        {
            cell->Set_cornerCoord(NE, cell->Get_ng(E)->Get_cornerCoord(NW));      /// NE corner is common with E ng's NW corner
            cell->Set_cornerCoord(NW, cell->Get_parent()->Get_cornerCoord(NW));   /// NW corner is common with parent's NW corner
            cell->Set_cornerCoord(SE, cell->Get_parent()->Get_centerCoord());     /// SE corner is common with parent's center

            cell->Get_cornerCoord(SW).Set_x(cell->Get_cornerCoord(NW).Get_x());   /// SWx = NWx
            cell->Get_cornerCoord(SW).Set_y(cell->Get_cornerCoord(SE).Get_y());   /// SWy = SEy
        }

        /// if I am the child[2] of my parent
        if(cell == cell->Get_parent()->Get_child(SW))
        {
            cell->Set_cornerCoord(NE, cell->Get_parent()->Get_centerCoord());     /// NE corner is common with parent's center
            cell->Set_cornerCoord(NW, cell->Get_ng(N)->Get_cornerCoord(SW));      /// NW corner is common with N ng's SW corner
            cell->Set_cornerCoord(SW, cell->Get_parent()->Get_cornerCoord(SW));   /// SW corner is common with parent's SW corner

            cell->Get_cornerCoord(SE).Set_x(cell->Get_cornerCoord(NE).Get_x());   /// SEx = NEx
            cell->Get_cornerCoord(SE).Set_y(cell->Get_cornerCoord(SW).Get_y());   /// SEy = SWy
        }

        /// if I am the child[3] of my parent
        if(cell == cell->Get_parent()->Get_child(SE))
        {
            cell->Set_cornerCoord(NE, cell->Get_ng(N)->Get_cornerCoord(SE));      /// NE corner is common with N ng's SE corner
            cell->Set_cornerCoord(NW, cell->Get_parent()->Get_centerCoord());     /// NW corner is common with parent's center
            cell->Set_cornerCoord(SW, cell->Get_ng(W)->Get_cornerCoord(SE));      /// SW corner is common with W ng's SE corner
            cell->Set_cornerCoord(SE, cell->Get_parent()->Get_cornerCoord(SE));   /// SE corner is common with parent's SE corner
        }

        ///If the cell has children, they are sent to findCorners(grid*)
        if (cell->Get_child(0) != NULL)
            for(int i=0; i<4; i++)
                findCorners(cell->Get_child(i));
    }
}


void preProcessor::inoutTest(grid *cell, inputOperations *IO)
{
/// ---------------------------------------------------------------------------
/// initializeClass::inoutTest
/// ---------------------------------------------------------------------------

    ///Only for computational cells
    if(cell->Get_level() == IO->Get_uniformDivision())
    {
        /// If a leaf cell is completely out of the bounding box of the geometry, it is set to out cell type
        /// directly. So, we dont need to make any more checks, this saves time.
        if( cell->Get_cornerCoord(SW).Get_x() > IO->Get_geometryMax().Get_x() || ///If left line is on the right side of x max of geometry or
            cell->Get_cornerCoord(SE).Get_x() < IO->Get_geometryMin().Get_x() || ///If right line is on the left side of x min of geometry or
            cell->Get_cornerCoord(SW).Get_y() > IO->Get_geometryMax().Get_y() || ///If bottom line is above the y max of geometry or
            cell->Get_cornerCoord(NW).Get_y() < IO->Get_geometryMin().Get_y()  ) ///If the top line is below the y min of geometry,
                cell->Set_cellType(typeOut);                                     ///then set this cell to an outside cell.

        /// If a leaf cell is completely or partially in the bounding box of the geometry, it is subject to the
        /// in/out test here(in 'else'). What happens is explained below:

        ///For all corners of the selected cell
            ///For all the polygons of the geometry
                ///For all points of the selected polygon
                    ///Our purpose is to send a ray from the selected corner in positive x direction and count
                    ///how many times this ray intersects the geometry boundaries. If the corner is in the geometry,
                    ///it must intersect the geometry 2n+1 times, if it is outside the geometry, it must intersect
                    ///the geometry 2n times.
                    ///To do this, the intersection point of the ray and the line connecting the two consecutive
                    ///points of the selected geometry is determined and compared with the x coordinate of the
                    ///selected corner.
                ///
            ///
        ///

        else
        {
            int mycorners[IO->Get_numberOfPolygons()][4];   /// temporarily holds corner type for each polygon (0 = outside, 1 = inside)
            for(int k=0; k<4; k++)  ///for each corner of the selected cell
            {
                for(int i=0; i<IO->Get_numberOfPolygons(); i++) ///on all the polygons of the geometry
                {
                    int numberOfCuts = 0;   ///this var holds how many times the ray sent from a corner intersects the geometry
                    float xcut, x0, x1, x2, y0, y1, y2, xlarge, xsmall, ylarge, ysmall;     ///these hold the coordinates of the points for easy access
                    for(int j=0; j<IO->Get_numberOfGeometryPoints(i); j++)  ///on all the points of the selected polygon of the geometry
                    {
                        ///Below, coordinates of the corner, whicha are x0, y0, and
                        ///coordinates of the two geometry points, which are x1,y1 and x2,y2 are determined.
                        ///This is done for easy access to these variables.
                        x0 = cell->Get_cornerCoord(k).Get_x();              ///x0 is the x coordinate of the corner
                        y0 = cell->Get_cornerCoord(k).Get_y();              ///y0 is the y coordinate of the corner
                        x1 = IO->Get_geometryPoints(i,j).Get_x();           ///x1 is the x coordinate of the first geometry point
                        y1 = IO->Get_geometryPoints(i,j).Get_y();           ///y1 is the y coordinate of the first geometry point
                        if ( j==IO->Get_numberOfGeometryPoints(i)-1 )       ///when the last point is reached, turns back to 0th point
                        {
                            x2 = IO->Get_geometryPoints(i,0).Get_x();       ///x2 is the x coordinate of the second geometry point
                            y2 = IO->Get_geometryPoints(i,0).Get_y();       ///y2 is the y coordiante of the second geometry point
                        }
                        else                                                ///when last point is not reached
                        {
                            x2 = IO->Get_geometryPoints(i,j+1).Get_x();     ///x2 is the x coordinate of the second geometry point
                            y2 = IO->Get_geometryPoints(i,j+1).Get_y();     ///y2 is the y coordiante of the second geometry point
                        }
                        ///Here the larger and smaller x and y coordiantes for the geometry points are determined.
                        ///These are necessary for the determination of cut point later on.
                        if (x1>x2)
                        {
                            xlarge = x1;
                            xsmall = x2;
                        }
                        else
                        {
                            xlarge = x2;
                            xsmall = x1;
                        }
                        if (y1>y2)
                        {
                            ylarge = y1;
                            ysmall = y2;
                        }
                        else
                        {
                            ylarge = y2;
                            ysmall = y1;
                        }
                        ///Below, cut location is determined if y0 is between the y values of the geometry points and
                        ///if at least one of the x coords of the geometry points are at the right side of the selected
                        ///corner. If x values of both of the geometry points are at the left side of the corner,
                        ///then it is impossible to cut it with a ray sent in positive x direction.
                        if(y0 <= ylarge && y0 >= ysmall)
                        {
                            if( x1 >= x0 || x2 >= x0)
                            {
                                ///intersection of y=y0 line and (y-y1)/(y1-y2) = (x-x1)(x1-x2) line gives
                                ///the x location of the cut.
                                xcut = (y0-y1)/(y1-y2)*(x1-x2)+x1;
                                ///if the cut location is between the x coordinates of the gemetry points, geometry is
                                ///cut. Otherwise, extensions of the ray and the geometry cut each other.
                                if (xcut <= xlarge && xcut >= xsmall && xcut >= x0)
                                {
                                    ///This var is increased for the selected corner for each intersection
                                    numberOfCuts++;
                                }
                            }
                        }
                    }
                    ///At this point we calculated how many times a ray sent in x direction from a specific corner
                    ///of a specific cell intersects a specific polygon!
                    ///If there are 2n intersections, then this corner is outside of the selected polygon.
                    if(numberOfCuts%2 == 0)
                        mycorners[i][k] = 0; ///0=outside. be used to determine inside outside condition of the cell.
                    ///If there are 2n+1 intersections, then this corner is inside of the selected polygon.
                    else
                        mycorners[i][k] = 1;    ///1=inside.
                }
                ///Here the procedure is completed for all polygons for a specific corner of a specific cell
            }
            ///At this point, in/out condition of the four corners of the selected cell are determined for each
            ///polygon. Now, in/out condition of the cell can be determined with the corner in/out information.
            ///Here is the idea: Sum up the mycorners[i][k] for each polygon, if sum is 0, the cell is outside;
            ///if sum is 4, cell is inside; if sum is between 0 and 4, the cell is cut.
            int m = 0;
            for(m=0; m<IO->Get_numberOfPolygons(); m++)
            {
                int cornercounter = 0;  ///used to store the summed corner condition
                for(int p=0; p<4; p++)  ///Corner conditons are summed for selected polygon
                    cornercounter = cornercounter + mycorners[m][p];
                if (cornercounter == 4)                         ///If the cell is inside of a polygon,
                {
                    cell->Set_cellType(typeIn);                 ///set it to an inside type.
                    break;                                      ///Then there is no need to chek for other polygons
                }
                else if (cornercounter>0 && cornercounter<4)    /// If the cell is cut by a polygon,
                {
                    cell->Set_cellType(typeCut);                ///Set it to a cut type
                    break;                                      ///There is no need to check for other polygons
                }
                else if(cornercounter == 0)                     ///If the cell is an outside cell
                    cell->Set_cellType(typeOut);///Set it to an outside type but keep checking for the other polygons
                                                ///The cell can be out of this polygon but can be inside or cut by
                                                ///other polygons. When the last polygon is reached, if the cell
                                                ///is really an outside cell, it will be marked as an outisde cell.
                else
                    cout << endl << "!!! Something is wrong with the corner in/out determination !!!" << endl
                                 << "See preProcessor::inoutTest function." << endl ;
            }
            ///At this point we determined the in/out condition of a specific cell using the previouly determined
            ///corner in/out conditions. Now, if we got a cutcell, we must change its type from grid to cutgrid.
            ///Using the cutgrid(cell) constructor, grid object and all its relations with other cells are copied
            ///to new cutgrid object easily. After the new cutgrid obj is created, old grid obj can be deleted.
            if(cell->Get_cellType() == typeCut)
            {
                cutgrid* cutcell = new cutgrid(cell);
                for(int q=0; q<4; q++)
                {
                    cutcell->Set_cornerType(q, mycorners[m][q]);
                }
                delete cell;
            }
        }
    }
    ///Keep searching until got a computational cell
    else
        for(int i=0; i<4; i++)
            inoutTest(cell->Get_child(i), IO);
}
