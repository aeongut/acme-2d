#ifndef COORD3D_H_INCLUDED
#define COORD3D_H_INCLUDED

class coord3D : public coord2D
{
    protected:
        float z;
    public:
        float   Get_z () {return z;}
//        float   Get_coord3D (int i) {if (i==0) return x; if (i==1) return y; if (i==2) return z;}   /// Returns with index: 0->x, 1->y, 2->z

        void    Set_z (float n_xyz) {z = n_xyz;}
//        void    Set_coord2D (float i, float n_xyz) {if (i==0) x=n_xyz; if (i==1) y=n_xyz; if (i==2) z=n_xyz;}    /// Sets with index: 0->x, 1->y, 2->z
};


#endif // COORD3D_H_INCLUDED
