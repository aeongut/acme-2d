#ifndef POSTPROCESSOR_H_INCLUDED
#define POSTPROCESSOR_H_INCLUDED

#include "grid.h"
#include "inputOperations.h"

#include "vtkActor.h"
#include "vtkCellArray.h"
#include "vtkDoubleArray.h"
#include "vtkFloatArray.h"
#include "vtkIntArray.h"
#include "vtkPointData.h"
#include "vtkPoints.h"
#include "vtkCellData.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkDataSetWriter.h"

class postProcessor
{
    private:
    int     countLeafCells(grid*);
    void    outputToVTKWriteMeshCoords(grid*, float**);
    void    outputToVTKWriteGeomCoords(inputOperations*, float**, int);
    void    outputToVTKWriteConnectivity(inputOperations*, vtkCellArray*, int);
    void    outputToVTKWriteProperty(grid*, vtkFloatArray*);

    public:
    void    outputToVTK(grid*, inputOperations*);
};

#endif // POSTPROCESSOR_H_INCLUDED
