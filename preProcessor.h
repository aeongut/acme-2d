#ifndef PREPROCESSOR_H_INCLUDED
#define PREPROCESSOR_H_INCLUDED

#include "grid.h"
#include "cutgrid.h"
#include "compgrid.h"
#include "inputOperations.h"

class preProcessor
{
    private:
        ///     Preprocessing functions ///
        void    initializeRoot (grid*,const float);     ///initializes the root cell: the mother of all
        void    uniformGrid (grid*,const float);        ///Generates the uniform mesh
        void    findNeighbours(grid*);                  ///Finds negibouring relations between all cells
        void    findCorners(grid*);                     ///Finds the corner coords of all cells
        void    inoutTest(grid*, inputOperations*);     ///Determines the cells inside/outside of and cut by geometry

    public:
        //ACCESS FUNCTION
        void    initializeControl(grid*, inputOperations*);
};

#endif // PREPROCESSOR_H_INCLUDED
