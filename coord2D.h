#ifndef COORD2D_H_INCLUDED
#define COORD2D_H_INCLUDED

/**
 * 2D node coordinates to be used by other classes
 */

class coord2D
{
    protected:
        float x,y;
    public:
        float   Get_x () {return x;}                                            /// Returns x
        float   Get_y () {return y;}                                            /// Returns y
//        float   Get_coord2D (int i) {if (i==0) return x; if (i==1) return y;}   /// Returns with index: 0->x, 1->y

        void    Set_x (float n_xyz) {x = n_xyz;}                                        /// Sets x
        void    Set_y (float n_xyz) {y = n_xyz;}                                        /// Sets x
//        void    Set_coord2D (float i, float n_xyz) {if (i==0) x=n_xyz; if (i==1) y=n_xyz;}    /// Sets with index: 0->x, 1->y
};

#endif // COORD2D_H_INCLUDED
