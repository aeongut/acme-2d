#include "grid.h"

/// DEFAULT CONSTRUCTOR
grid::grid()
{
    cellID      = 0;
    cellType    = 0;                    ///Set to outside cell by default
    level       = 0;
    length      = 0.0f;
    property    = 0.0f;
    parent      = NULL;
    centerCoord.Set_x(0.0f);
    centerCoord.Set_y(0.0f);
    for(int i=0; i<4; i++)
    {
        child[i]        = NULL;
        ng[i]           = NULL;
        cornerCoord[i].Set_x(0.0f);
        cornerCoord[i].Set_y(0.0f);
    }
}

/** COPY CONSTRUCTOR
    This constructor takes a grid object or an object of a derived class (old cell) and copies all its properties
    to a grid or derived class object(new cell). Parent, children and neighbouring info are copied at address level,
    corner, center coords and other properties are copied at value level.
    Moreover, parent, children and ng's pointing to the old cell are changed such that they point the new cell.
    1- With copy constructor any two base or derived class object can be copied between each other.
    2- You can consider argument grid* object as the old cell and 'this' pointer as the new cell.
    3- Don't forget that pointers to base and derived classes are interchangable.
**/
grid::grid(grid* cell)
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// At this part, properties of the old cell are copied to the new cell.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    this->cellID      = cell->Get_cellID();       ///Copies the value of old cell cellID to new cell cellID
    this->cellType    = cell->Get_cellType();     ///Copies the value of old cell cellType to new cell cellType
    this->level       = cell->Get_level();        ///Copies the value of old cell level to new cell level
    this->length      = cell->Get_length();       ///Copies the value of old cell length to new cell length
    this->property    = cell->Get_property();     ///Copies the value of old cell property to new cell property
    this->parent      = cell->Get_parent();       ///Copies the address of old cell parent to new cell parent
    this->centerCoord.Set_x(cell->Get_centerCoord().Get_x());     ///Copies center x value
    this->centerCoord.Set_y(cell->Get_centerCoord().Get_y());     ///Copies center y value

    ///This loop is for 4 children, 4 neighbours and 4 corners.
    for(int i=0; i<4; i++)
    {
        ///Derived objects should have no children. For ex. a cutgrid or a compgrid object is a leaf cell.
        if(cell->Get_child(i) == NULL)              ///If cell doesn't have a child,
            this->child[i] = NULL;                  ///cutcell doesn't too. This is the normal condition.
        else                                        ///If cell has children,
            this->child[i] = cell->Get_child(i);    ///their addresses are copied to cutgrid children

        ///Copies the address of the neigbours of grid object to derived object
        this->ng[i] = cell->Get_ng(i);

        ///Copies the corner coordinates of grid object to derived object
        this->cornerCoord[i].Set_x(cell->Get_cornerCoord(i).Get_x()); ///Copies corner x value
        this->cornerCoord[i].Set_y(cell->Get_cornerCoord(i).Get_y()); ///Copies corner y value
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// At this part, cells pointing to the old cell as their parent, children or ng are changed such that
/// they point to the new cell now on.
//////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///The parent of the old cell now points to the new cell as its child.
    for(int q=0; q<4; q++)
    {
        if(cell == cell->Get_parent()->Get_child(q))
            cell->Get_parent()->Set_child(q, this);
    }

    ///The neighbours of the old cell now point to the new cell as thier neighbours.
    if(cell->Get_ng(E) != NULL)
    {
        if(cell->Get_ng(E)->Get_child(NW) != NULL)
        {
            cell->Get_ng(E)->Get_child(NW)->Set_ng(W, this);
            cell->Get_ng(E)->Get_child(SW)->Set_ng(W, this);
        }
        else
            cell->Get_ng(E)->Set_ng(W, this);
    }
    if(cell->Get_ng(N) != NULL)
    {
        if(cell->Get_ng(N)->Get_child(SW) != NULL)
        {
            cell->Get_ng(N)->Get_child(SW)->Set_ng(S, this);
            cell->Get_ng(N)->Get_child(SE)->Set_ng(S, this);
        }
        else
            cell->Get_ng(N)->Set_ng(S, this);
    }
    if(cell->Get_ng(W) != NULL)
    {
        if(cell->Get_ng(W)->Get_child(NE) != NULL)
        {
            cell->Get_ng(W)->Get_child(NE)->Set_ng(E, this);
            cell->Get_ng(W)->Get_child(SE)->Set_ng(E, this);
        }
        else
            cell->Get_ng(W)->Set_ng(E, this);
    }
    if(cell->Get_ng(S) != NULL)
    {
        if(cell->Get_ng(S)->Get_child(NW) != NULL)
        {
            cell->Get_ng(S)->Get_child(NW)->Set_ng(N, this);
            cell->Get_ng(S)->Get_child(NE)->Set_ng(N, this);
        }
        else
            cell->Get_ng(S)->Set_ng(N, this);
    }

    ///Children of the old cell now point to the new cell as their parent
    if(cell->Get_child(0) != NULL)                  ///If old cell has a child,
        for(int i=0; i<4; i++)                      ///for its all children
            cell->Get_child(i)->Set_parent(this);   ///set the parent of the children of the old cell to new cell.
}

/// DESTRUCTOR
grid::~grid()
{
    /** We can delete the memory for parent, children and neighbours, but other cells need this data.
     ** If we delete them, this will result in chain deep copying of all the connected objects.
     ** Therefore, while deleting a grid object, this condition must be taken into account.
     **/
    parent = NULL;              ///Parent pointer is set to null
    for(int i=0; i<4; i++)
    {
        child[i]    = NULL;     ///Child pointers are set to null
        ng[i]       = NULL;     ///Neighbour pointers are set to null
    }
}
