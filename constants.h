#ifndef CONSTANTS_H_INCLUDED
#define CONSTANTS_H_INCLUDED

#define PI 3.14159265

const int E = 0;
const int N = 1;
const int W = 2;
const int S = 3;

const int NE = 0;
const int NW = 1;
const int SW = 2;
const int SE = 3;

const int typeOut   = 0;
const int typeIn    = 1;
const int typeCut   = 2;

#endif // CONSTANTS_H_INCLUDED
