# 2d Automatic Cartesian Mesh Generator

This is a code to generate Cartesian meshes based on quadtree data structure
and simple cut cells on the solid boundaries. I generated a nice data structure
for the mesh but there are still a lot of things to do.

## Current Capabilities
* Uniform mesh generation to desired depth
* Import geometry as vtk polygons
* Export mesh in vtk format using vtk library
* Cut-cell determination algorithm (in/out test)

## Soon to be added
* Geometric and solution refinement
* Improvement of data structures
* Data-parallel distribution of the mesh

## Examples
Here is an example showing the useful parts of the cut cells.
![meshNaca0012.png](https://bitbucket.org/repo/ynKpBX/images/1900386198-meshNaca0012.png)